# x3dtest

This README outlines the details of collaborating on this Ember application.
A short introduction of this app could easily go here.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)
* [Google Chrome](https://google.com/chrome/)

## Installation

* `git clone <repository-url>` this repository
* `cd x3dtest`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

## Handling to get a error

* at the beginning the browser should rendered a cuboid and a plane in a x3d scene, the display value is set to true
* pressing the 'toggle display' button sets the display value false
* the x3d node for the plane will removed from the html code
* the plane will no longer rendered
* pressing the 'toggle display' button again sets the display value true
* the same x3d node as above will append to the html code but the plane will not displayed in the browser x3d scene
* the browser console show an error: Uncaught TypeError: Cannot read property 'getPoints' of null
    at x3dom.registerNodeType.defineClass.nodeChanged.nodeChanged (x3dom-full.js:4596)
* for comparison: by pressing the button 'add object by append' an other x3d node will added to the x3d scene by a jquery commad without an error
* so it should be possible to add scene nodes with out errors
* because I want to add and remove different x3d elements several times, it is easier to use the ember behavior, so I need to fix the error

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Linting

* `npm run lint:hbs`
* `npm run lint:js`
* `npm run lint:js -- --fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
