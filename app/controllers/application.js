import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import Controller from '@ember/controller';
import Component from '@ember/component';
import { action, computed } from '@ember/object';
import { later } from '@ember/runloop';

export default Controller.extend({

  display: true,

  @action
  setDisplay(value){

    this.set("display", !value);

  },

  @action
  addObjectByAppend(){
    $("#scene1").append("<transform scaleorientation=\"0,0,0,0\" scale=\"1,1,1\" center=\"0 0 0\" bboxsize=\"-1,-1,-1\" bboxcenter=\"0 0 0\" rotation=\"0 1 0 3.141592 \" translation=\"0 0 0\" id=\"0\" class=\"testClassNameXXX ember-view\" render=\"true\"><shape def=\"\" ispickable=\"0\" bboxsize=\"-1,-1,-1\" bboxcenter=\"0,0,0\" id=\"ember1546\" class=\"ember-view\" render=\"true\"><appearance sorttype=\"auto\" id=\"ember1547\" class=\"ember-view\" alphaclipthreshold=\"0.1\"><material specularcolor=\"0.52 0.385 0.02\" shininess=\"0.2\" emissivecolor=\"0.52 0.385 0.02\" ambientintensity=\"0.2\" transparency=\"0.65\" diffusecolor=\"0.52 0.385 0.02\" id=\"ember1548\" class=\"ember-view\"></material></appearance><indexedfaceset ccw=\"true\" colorpervertex=\"false\" colorindex=\" 0 0 0 0 0 0\" coordindex=\" 0 3 2 1 0 -1 1 2 6 5 1 -1 4 5 6 7 4 -1 0 4 7 3 0 -1 0 1 5 4 0 -1 2 3 7 6 2 -1 \" id=\"ember1549\" class=\"testClassNameXXX ember-view\" solid=\"true\" usegeocache=\"true\" lit=\"false\" normalpervertex=\"true\" normalupdatemode=\"fast\" convex=\"true\" normalindex=\"0\" texcoordindex=\"0\"><coordinate point=\"0 2.2 0 0 0 0 4.3 0 0 4.3 2.2 0 0 2.2 1.2 0 0 1.2 4.3 0 1.2 4.3 2.2 1.2 \"></coordinate><color color=\"0.52 0.385 0.02\"></color></indexedfaceset></shape><shape><indexedlineSet coordIndex=\"0 3 2 1 0 -1 1 2 6 5 1 -1 4 5 6 7 4 -1 0 4 7 3 0 -1 0 1 5 4 0 -1 2 3 7 6 2 -1\"><coordinate point=\"0 2.2 0 0 0 0 4.3 0 0 4.3 2.2 0 0 2.2 1.2 0 0 1.2 4.3 0 1.2 4.3 2.2 1.2\"/></indexedlineSet></shape></transform>")

  }

});
